// Importar el framework express
import express, { Request, Response} from 'express';
require('dotenv').config();

const app = express();
const port = process.env.PORT || 3000;

//Rutas
//Ruta -> home
app.get('/', (req: Request, res: Response) =>{
    res.send('Welcome to SENA Dosquebradas / Colombia');
})

app.listen(port, () => {
    console.log(`App listening on ${port}`);
});